using System.Text;
using System.Data.OracleClient;

namespace OraDump
{
	public class OracleClient
	{
		public enum ExportType {
			PackageHeader,
			PackageBody
		}

		// vytvoreni konexe do db
		private static OracleConnection GetConnection(string databaseConnection, string databaseUsername, string databasePassword) 
		{
            // (DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=MyHost)(PORT=MyPort))(CONNECT_DATA=(SERVICE_NAME=MyOracleSID)))
			string connectionString = "Data Source=" + databaseConnection + "; User ID=" + databaseUsername + "; Password=" + databasePassword +";";
			return new OracleConnection(connectionString);
		}

		public static string ExportPackage(ExportItem item, Export export, ExportType type)
		{
			OracleConnection dbcon = GetConnection(export.DatabaseConnection, export.DatabaseUsername, export.DatabasePassword);
			dbcon.Open ();

			OracleCommand dbcmd = dbcon.CreateCommand();
			string typeStr = type == ExportType.PackageHeader ? "PACKAGE" : "PACKAGE BODY";
            dbcmd.CommandText = "select text as result from all_source where owner=upper('" + export.DatabaseUsername + "') and type='" + typeStr + "' and name=upper('" + item.PackageName + "')  order by line asc";

			OracleDataReader reader = dbcmd.ExecuteReader();

			StringBuilder result = new StringBuilder();

            result.AppendLine("CREATE OR REPLACE");

			while (reader.Read ()) {
		    	result.Append((string) reader["result"]);
		   	}

            result.AppendLine("");
            result.AppendLine("/");
            result.AppendLine("");

	       	reader.Close ();
			dbcon.Close ();

			return result.ToString ();
		}

		public static void GetPackageList(Export export)
		{
			OracleConnection dbcon = GetConnection(export.DatabaseConnection, export.DatabaseUsername, export.DatabasePassword);
			dbcon.Open ();


			OracleCommand dbcmd = dbcon.CreateCommand();
            dbcmd.CommandText = "select object_name, to_char(LAST_DDL_TIME, 'yyyy-mm-dd hh24:mi') as LAST_DDL_TIME from dba_objects where object_type='PACKAGE BODY' and owner=upper('" + export.DatabaseUsername + "')";
	       
			OracleDataReader reader = dbcmd.ExecuteReader();

			// nulovani timestamp
			foreach(ExportItem item in export.Items)
			{
				item.Timestamp = string.Empty;
			}

			while (reader.Read ()) {
		    	string packageName = (string) reader["object_name"];
                string timestamp = (string)reader["LAST_DDL_TIME"];

				bool found = false;
				foreach (ExportItem item in export.Items)
				{
					if (item.PackageName == packageName)
					{
						found = true;
						item.Timestamp = timestamp;
						break;
					}
				}

				if (!found)
				{
					ExportItem item = new ExportItem(true, packageName, "") {Timestamp = timestamp};
					export.Items.Add(item);
				}
		   	}

			// odstraneni nepouzitych
			export.Items.RemoveAll(x => x.Timestamp == string.Empty);
	       
	       	reader.Close ();
			dbcon.Close ();
    	}
	}
}

