namespace OraDump
{
	public class ExportItem 
	{
		public bool IsExported {get;set;}
		public string PackageName {get;set;}
		public string ExportDirectory {get;set;}
		public string Timestamp {get;set;}
		
		public ExportItem()
		{
		
		}
		
		public ExportItem(bool isExported, string packageName, string exportDirectory)
		{
			IsExported = isExported;
			PackageName = packageName;
			ExportDirectory = exportDirectory;
			Timestamp = string.Empty;
		}
	}
}

