namespace OraDump
{
	public class ConfigurationFile
	{
		// singleton
		ConfigurationFile instance;
		ConfigurationFile Instance 
		{
			get { return instance ?? (instance = new ConfigurationFile()); }
		}
		
		// private konstruktor
		private ConfigurationFile()
		{
			
		}
	}
}

