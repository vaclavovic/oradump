using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using Gtk;

namespace OraDump
{
	public class SourceManager
	{
		public Export Export { get; set; }
		public string Filename { get; set; }
		
		// singleton
		private static SourceManager instance;
		public static SourceManager Instance { get { return instance ?? (instance = new SourceManager()); } }
		
		// private konstruktor
		private SourceManager() 
		{ 
			Export = new Export
				         {
					         DatabaseConnection = string.Empty,
					         DatabaseUsername = string.Empty,
					         DatabasePassword = string.Empty,
					         RootDirectory = string.Empty,
					         Items = new List<ExportItem>()
				         };

			Filename = string.Empty;
		}
		
		// export do struktury pro zobrazeni
		public ListStore ExportToListStore() 
		{
			ListStore store = new ListStore(typeof(bool), typeof(string), typeof(string), typeof(string));

			foreach(ExportItem item in Export.Items)
			{
				store.AppendValues(item.IsExported, item.PackageName, item.ExportDirectory, item.Timestamp);
			}

			return store;
		}
		
		// import ze zobrazovací struktury 
		public void ImportFromListStore(ListStore ls) 
		{
			TreeIter iter;
			Export.Items = new List<ExportItem>();

			if (ls.GetIterFirst(out iter)) {
				do {
					ExportItem item = new ExportItem((bool)ls.GetValue(iter, 0),(string)ls.GetValue(iter, 1),(string)ls.GetValue(iter, 2));	
					Export.Items.Add(item);
				} while (ls.IterNext(ref iter));
			}
		}

		// serializace do xml a ulozeni do souboru
		public void ExportToXml(string filename) 
		{
			Filename = filename;

			XmlSerializer ser = new XmlSerializer(typeof(Export));		
			TextWriter writer = new StreamWriter(filename);
    		ser.Serialize(writer, Export);
    		writer.Close();
		}
		
		// deserializace a nacteni ze souboru
		public bool ImportFromXml (string filename)
		{
			try 
			{
				Filename = filename;
			
				XmlSerializer ser = new XmlSerializer (typeof(Export));
				FileStream file = new FileStream (filename, FileMode.Open);
				Export = (Export)ser.Deserialize (file);
				file.Close();

				Export.Items = Export.Items.OrderBy(o => o.PackageName).ToList();
				return true;
			} 
			catch 
			{
				return false;
			}
		}
	}
}

