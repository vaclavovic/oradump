using System;
using Gtk;
using System.Reflection;
using System.IO;

namespace OraDump
{
	public sealed partial class MainWindow: Window
	{	
		private ListStore model;
		private const string extension = ".oradump";

		public MainWindow (): base (WindowType.Toplevel)
		{
			Build();

			//OraDump.OracleClient.TestConnection();

			// renderee pro checkbox
			CellRendererToggle crt = new CellRendererToggle {Activatable = true};
			crt.Toggled += OnCellToggled;

			// renderer pro editaci adresáře
			CellRendererText renderer = new CellRendererText {Editable = true};
			renderer.Edited += OnCellEdited;
			
			// definice sloupců
			nodeView.AppendColumn("Export", crt, "active", 0);
			nodeView.AppendColumn("Package", new CellRendererText (), "text", 1);
			nodeView.AppendColumn("Directory", renderer, "text", 2);
			nodeView.AppendColumn("Timestamp", new CellRendererText (), "text", 3);

			// nastaveni trideni sloupcu
			nodeView.GetColumn(0).SortColumnId = 0;
			nodeView.GetColumn(1).SortColumnId = 1;
			nodeView.GetColumn(2).SortColumnId = 2;	 
			nodeView.GetColumn(3).SortColumnId = 3;	 

			// nacteni a zobrazeni dat
			nodeView.Model = model = SourceManager.Instance.ExportToListStore(); 
			nodeView.ShowAll();
		}

		
		private void OnCellEdited(object o, EditedArgs args)
	    {
			TreeIter iter;
			
			if (model.GetIterFromString(out iter, args.Path)) {
				model.SetValue(iter, 2, args.NewText);
			}
	       
	        nodeView.ColumnsAutosize();
	    }
	
		private void OnCellToggled(object o, ToggledArgs args)
	    {
			TreeIter iter;
			
			if (model.GetIterFromString(out iter, args.Path)) {
				bool old = (bool)model.GetValue(iter, 0);
				model.SetValue(iter, 0, !old);
			}
	    }

		private void OnDeleteEvent (object sender, DeleteEventArgs a)
		{
			Application.Quit ();
			a.RetVal = true;
		}

		private void OnDialogInfoActionActivated (object sender, EventArgs e)
		{
			AboutDialog dialog = new AboutDialog();
			Assembly asm = Assembly.GetExecutingAssembly ();
		
			dialog.ProgramName = (asm.GetCustomAttributes (
				typeof (AssemblyTitleAttribute), false) [0]
				as AssemblyTitleAttribute).Title;
		
			dialog.Version = asm.GetName().Version.ToString ();
		
			dialog.Comments = (asm.GetCustomAttributes (
				typeof (AssemblyDescriptionAttribute), false) [0]
				as AssemblyDescriptionAttribute).Description;
		
			dialog.Copyright = (asm.GetCustomAttributes (
				typeof (AssemblyCopyrightAttribute), false) [0]
				as AssemblyCopyrightAttribute).Copyright;
			
			dialog.Website = "https://bitbucket.org/vaclavovic/oradump";
		
			dialog.Run();
			dialog.Destroy();
		}

		private void OnExecuteActionActivated (object sender, EventArgs e)
		{
			MessageDialog md = null;
			statusbar4.Push(1, "Packages export...");

			try 
			{
                int total = 0;
                int done = 0;

                foreach (ExportItem item in SourceManager.Instance.Export.Items)
                {
                    if (!item.IsExported) continue;
                    total++;
                }

				foreach(ExportItem item in SourceManager.Instance.Export.Items)
				{
                    // Flush pending events to keep the GUI reponsive
                    while (Gtk.Application.EventsPending())
                        Gtk.Application.RunIteration();

					if (!item.IsExported) continue;

                    done++;

					string packageHeader = OracleClient.ExportPackage(item, SourceManager.Instance.Export, OracleClient.ExportType.PackageHeader);
					string packageBody = OracleClient.ExportPackage(item, SourceManager.Instance.Export, OracleClient.ExportType.PackageBody);
				
					string directory = SourceManager.Instance.Export.RootDirectory + "/" + item.ExportDirectory + "/";
					Directory.CreateDirectory(directory);

                    statusbar4.Push(1, "Exporting " + item.PackageName.ToUpper() + " header (" + done + "/" + total + ")");

					using (StreamWriter outfile = new StreamWriter(directory + item.PackageName.ToUpper() + "_header.sql", false))
					{
                        // Flush pending events to keep the GUI reponsive
                        while (Gtk.Application.EventsPending())
                            Gtk.Application.RunIteration();

						outfile.Write(packageHeader);
					}

                    statusbar4.Push(1, "Exporting " + item.PackageName.ToUpper() + " body (" + done + "/" + total + ")");

					using (StreamWriter outfile = new StreamWriter(directory + item.PackageName.ToUpper() + "_body.sql", false))
					{
                        // Flush pending events to keep the GUI reponsive
                        while (Gtk.Application.EventsPending())
                            Gtk.Application.RunIteration();

						outfile.Write(packageBody);
					}
				}

				statusbar4.Push(1, "Packages export finished.");
				md = new MessageDialog (null, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, "Export finished");
			} 
			catch (Exception exc) 
			{
				statusbar4.Push(1, "Packages export failed.");
				md = new MessageDialog (null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, exc.Message);
			}
			finally 
			{
				if (md != null)
				{
					md.Run();
					md.Destroy();
				}
			}
		}

		private void OnRefreshActionActivated (object sender, EventArgs e)
		{
			MessageDialog md = null;
			statusbar4.Push(1, "Packages export...");

			try
			{
				OracleClient.GetPackageList(SourceManager.Instance.Export);
				nodeView.Model = model = SourceManager.Instance.ExportToListStore();

				statusbar4.Push(1, "Packages list has been loaded from database.");
			}
			catch (Exception exc)
			{
				md = new MessageDialog(null, DialogFlags.Modal, MessageType.Error, ButtonsType.Ok, exc.Message);
				statusbar4.Push(1, "Packages list refresh failed.");
			}
			finally
			{
				if (md != null)
				{
					md.Run();
					md.Destroy();
				}
			}
		}

		private void OnSaveAsAction1Activated(object sender, EventArgs e)
		{
			FileChooserDialog fc = new FileChooserDialog("Save export configuration as...", this,
		    	FileChooserAction.Save, Stock.Cancel, ResponseType.Cancel, Stock.Save, ResponseType.Accept);

			fc.SetFilename(SourceManager.Instance.Filename);

			if (fc.Run() == (int) ResponseType.Accept) 
			{
				string filename = fc.Filename;
				if (!filename.EndsWith(extension))
				{
					filename += extension;
				}
				SourceManager.Instance.ImportFromListStore((ListStore)nodeView.Model);
				SourceManager.Instance.ExportToXml(filename);	
				statusbar4.Push(1, "Export configuration has been saved.");

				Title = "OraDump (" + fc.Filename + ")";
			}
		
			fc.Destroy();
		}

		private void OnOpenAction1Activated (object sender, EventArgs e)
		{
			FileChooserDialog fc = new FileChooserDialog("Choose export configuration to open...",
		                            this,
		                            FileChooserAction.Open,
		                            Stock.Cancel, ResponseType.Cancel,
		                            Stock.Open, ResponseType.Accept);
			
			FileFilter filter = new FileFilter {Name = "OraDump export files (*" + extension + ")"};
			filter.AddPattern("*" + extension);
			fc.AddFilter(filter);

			if (fc.Run() == (int) ResponseType.Accept) 
			{
				if (!SourceManager.Instance.ImportFromXml(fc.Filename))
				{
					statusbar4.Push(1, "Export configuration loading failed.");
					fc.Destroy();
					return;
				}
				nodeView.Model = model = SourceManager.Instance.ExportToListStore();
	        	
				enConnection.Text = SourceManager.Instance.Export.DatabaseConnection;
				enPassword.Text = SourceManager.Instance.Export.DatabasePassword;
				enUsername.Text = SourceManager.Instance.Export.DatabaseUsername;

				filechooserbutton2.SetCurrentFolder(SourceManager.Instance.Export.RootDirectory);
				statusbar4.Push(1, "Export configuration has been loaded.");

				Title = "OraDump (" + fc.Filename + ")";
			}

			fc.Destroy();
		}

		private void OnFilechooserbutton2SelectionChanged (object sender, EventArgs e)
		{
			SourceManager.Instance.Export.RootDirectory = filechooserbutton2.CurrentFolder;
		}

		// nastaveni connect stringu
		private void OnEnConnectionChanged (object sender, EventArgs e)
		{
			SourceManager.Instance.Export.DatabaseConnection = enConnection.Text;
		}

		// nastaveni hesla
		private void OnEnPasswordChanged (object sender, EventArgs e)
		{
			SourceManager.Instance.Export.DatabasePassword = enPassword.Text;
		}

		// nastaveni username
		private void OnEnUsernameChanged (object sender, EventArgs e)
		{
			SourceManager.Instance.Export.DatabaseUsername = enUsername.Text;
		}

	}
}
