using System.Collections.Generic;

namespace OraDump
{
	public class Export 
	{
		public string RootDirectory {get;set;}

		public string DatabaseConnection {get;set;}
		public string DatabaseUsername {get;set;}
		public string DatabasePassword {get;set;}

		public List<ExportItem> Items {get;set;}
	}
}

